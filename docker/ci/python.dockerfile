FROM python:3.10-slim-bullseye as packages

RUN pip install "pipenv==2022.8.5"

WORKDIR /pipenv

COPY Pipfile Pipfile.lock ./
RUN pipenv requirements --dev > requirements.txt

FROM python:3.10-slim-bullseye

ENV PYTHONUNBUFFERED=0

RUN apt-get update --yes --quiet \
    && apt-get install --yes --quiet --no-install-recommends \
        build-essential \
        git \
    && rm -rf /var/lib/apt/lists/* \

WORKDIR /repository

# Install packages
COPY --from=packages /pipenv/requirements.txt .
RUN pip install -r requirements.txt
