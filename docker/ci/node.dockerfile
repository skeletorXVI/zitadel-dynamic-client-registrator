FROM node:16-buster-slim

WORKDIR /packages

COPY .yarnrc.yml package.json yarn.lock ./
COPY .yarn/releases .yarn/releases

RUN yarn install
