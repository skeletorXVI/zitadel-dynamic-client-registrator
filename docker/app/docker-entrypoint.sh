#!/usr/bin/env bash

# Based on https://github.com/nginxinc/docker-nginx/blob/master/stable/debian/docker-entrypoint.sh

set -e

if [ -z "${ENTRYPOINT_QUIET_LOGS:-}" ]; then
    exec 3>&1
else
    exec 3>/dev/null
fi

run_file()
{
  if [ -x "$1" ]; then
        echo >&3 "$0: Launching $1";
        "$1"
    else
        # warn on shell scripts without exec bit
        echo >&3 "$0: Ignoring $1; not executable";
    fi
}

if [ "$1" = "gunicorn" ]; then
    # shellcheck disable=SC2034,SC2162
    if /usr/bin/find "/docker-entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read r; then
        echo >&3 "$0: /docker-entrypoint.d/ is not empty, will attempt to perform configuration"

        echo >&3 "$0: Looking for shell and python scripts in /docker-entrypoint.d/"
        find "/docker-entrypoint.d/" -follow -type f -print | sort -V | while read -r f; do
            case "$f" in
                *.sh) run_file "$f";;
                *.py) run_file "$f";;
                *) echo >&3 "$0: Ignoring $f";;
            esac
        done

        echo >&3 "$0: Configuration complete; ready for start up"
    else
        echo >&3 "$0: No files found in /docker-entrypoint.d/, skipping configuration"
    fi
fi

exec "$@"
