import json
import time

from jose import jwt

from zitadel_dynamic_client_registrator.settings import get_settings


def generate_jwt() -> str:
    with open(get_settings().key_file) as file:
        key_data = json.load(file)
    timestamp = int(time.time())
    token = jwt.encode(
        {
            "sub": key_data["userId"],
            "iat": timestamp,
            "aud": get_settings().sso_base_url,
            "iss": key_data["userId"],
            "exp": timestamp + 3600,
        },
        key_data["key"],
        algorithm="RS256",
        headers={"kid": key_data["keyId"]},
    )
    return token
