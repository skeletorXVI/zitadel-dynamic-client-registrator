from httpx import AsyncClient
from starlette import status

from zitadel_dynamic_client_registrator.auth import generate_jwt
from zitadel_dynamic_client_registrator.exceptions import (
    AppWithNameExistsAlready,
    AuthenticationFailed,
    UnableToRetrieveOpenIDConfiguration,
    UnexpectedResponse,
)
from zitadel_dynamic_client_registrator.settings import get_settings
from zitadel_dynamic_client_registrator.tools import build_url
from zitadel_dynamic_client_registrator.types import (
    CreateOIDCClientResponse,
    OIDCClient,
)


async def get_bearer_token(client: AsyncClient) -> str:
    jwt = generate_jwt()
    url = build_url("/oauth/v2/token")
    response = await client.post(
        url,
        data={
            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
            "scope": "urn:zitadel:iam:org:project:id:zitadel:aud",
            "assertion": jwt,
        },
    )
    match response.status_code:
        case status.HTTP_200_OK:
            return response.json()["access_token"]
        case _:
            raise AuthenticationFailed()


async def get_openid_configuration() -> dict:
    async with AsyncClient() as client:
        response = await client.get(
            build_url(get_settings().path_prefix, "/.well-known/openid-configuration")
        )
    match response.status_code:
        case status.HTTP_200_OK:
            return response.json()
        case _:
            raise UnableToRetrieveOpenIDConfiguration(response)


async def add_oidc_client(oidc_client: OIDCClient) -> CreateOIDCClientResponse:
    async with AsyncClient() as client:
        bearer_token = await get_bearer_token(client)
        response = await client.post(
            build_url(f"/management/v1/projects/{oidc_client.project_id}/apps/oidc"),
            headers={"Authorization": f"Bearer {bearer_token}"},
            json=oidc_client.dict(by_alias=True),
        )

    match response.status_code:
        case status.HTTP_200_OK:
            return CreateOIDCClientResponse.parse_obj(response.json())
        case status.HTTP_409_CONFLICT:
            raise AppWithNameExistsAlready()
        case _:
            raise UnexpectedResponse()
