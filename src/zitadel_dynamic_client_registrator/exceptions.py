class ZitadelDynamicClientRegistratorException(Exception):
    pass


class ResponseException(ZitadelDynamicClientRegistratorException):
    pass


class UnexpectedResponse(ResponseException):
    pass


class UnableToRetrieveOpenIDConfiguration(ResponseException):
    pass


class AuthenticationFailed(ResponseException):
    pass


class AppWithNameExistsAlready(ResponseException):
    pass


class ExhaustedAppRegistrationRetries(ZitadelDynamicClientRegistratorException):
    pass
