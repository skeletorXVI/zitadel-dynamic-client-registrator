import datetime
from enum import Enum

from humps.main import camelize
from pydantic import AnyUrl, BaseModel, Extra, Field, PositiveInt


class OIDCResponseType(str, Enum):
    code = "OIDC_RESPONSE_TYPE_CODE"
    id_token = "OIDC_RESPONSE_TYPE_ID_TOKEN"
    id_token_token = "OIDC_RESPONSE_TYPE_ID_TOKEN_TOKEN"


class OIDCGrantType(str, Enum):
    authorization_code = "OIDC_GRANT_TYPE_AUTHORIZATION_CODE"
    implicit = "OIDC_GRANT_TYPE_IMPLICIT"
    refresh_token = "OIDC_GRANT_TYPE_REFRESH_TOKEN"


class OIDCAppType(str, Enum):
    web = "OIDC_APP_TYPE_WEB"
    user_agent = "OIDC_APP_TYPE_USER_AGENT"
    native = "OIDC_APP_TYPE_NATIVE"


class OIDCAuthMethod(str, Enum):
    basic = "OIDC_AUTH_METHOD_TYPE_BASIC"
    post = "OIDC_AUTH_METHOD_TYPE_POST"
    none = "OIDC_AUTH_METHOD_TYPE_NONE"
    private_key_jwt = "OIDC_AUTH_METHOD_TYPE_PRIVATE_KEY_JWT"


class OIDCVersion(str, Enum):
    v1_0 = "OIDC_VERSION_1_0"


class OIDCAccessTokenType(str, Enum):
    bearer = "OIDC_TOKEN_TYPE_BEARER"
    jwt = "OIDC_TOKEN_TYPE_JWT"


class CamelCaseModel(BaseModel):
    class Config:
        allow_population_by_field_name = True
        alias_generator = camelize


class OIDCClient(CamelCaseModel):
    project_id: str = Field(alias="projectID")
    name: str
    redirect_uris: list[AnyUrl]
    response_types: list[OIDCResponseType]
    grant_types: list[OIDCGrantType]
    app_type: OIDCAppType
    auth_method_type: OIDCAuthMethod
    post_logout_redirect_uris: list[AnyUrl]
    version: OIDCVersion
    dev_mode: bool
    access_token_type: OIDCAccessTokenType
    access_token_role_assertion: bool
    id_token_role_assertion: bool
    id_token_userinfo_assertion: bool
    clock_skew: str
    additional_origins: list[str]


class CreateOIDCClientDetailsResponse(CamelCaseModel):
    sequence: PositiveInt
    creation_date: datetime.datetime
    resource_owner: str


class CreateOIDCClientComplianceProblemResponse(CamelCaseModel):
    key: str
    localized_message: str


class CreateOIDCClientResponse(CamelCaseModel):
    app_id: str
    details: CreateOIDCClientDetailsResponse
    client_id: str
    client_secret: str = ""
    none_compliant: bool = False
    compliance_problem: list[CreateOIDCClientComplianceProblemResponse] = []


class ErrorDetailsResponse(CamelCaseModel):
    type_url: str
    value: str


class ErrorResponse(CamelCaseModel):
    code: int
    message: str
    details: list[ErrorDetailsResponse]


class RegisterIn(BaseModel):
    application_type: str
    client_name: str
    redirect_uris: list[AnyUrl]
    token_endpoint_auth_method: str

    class Config:
        extra = Extra.allow


class RegisterOut(BaseModel):
    client_id: str
    client_secret: str
    client_secret_expires_at: int
    redirect_uris: list[AnyUrl]
    grant_types: list[str]
