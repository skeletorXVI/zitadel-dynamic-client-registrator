import json
import textwrap
from unittest.mock import mock_open, patch

import pytest

from zitadel_dynamic_client_registrator.auth import generate_jwt
from zitadel_dynamic_client_registrator.settings import AppSettings, ClientProjectMapper


@pytest.mark.parametrize(
    "app_settings, key_data, time, expected",
    [
        (
            AppSettings(
                sso_base_url="https://zitadel.example.com",
                key_file="secrets/credentials.json",
                path_prefix="origin",
                client_project_mappers=[
                    ClientProjectMapper(
                        regex=r"^.*(ownCloud).*$",
                        project_id="180582120094172208",
                    )
                ],
            ),
            {
                "type": "serviceaccount",
                "keyId": "987654321987654321",
                "key": textwrap.dedent(
                    """\
                        -----BEGIN RSA PRIVATE KEY-----
                        MIIEowIBAAKCAQEAvGM9Tf3hvJj1uQ11VbC1Q8h/eZ0F3W90O64n+0ZwNvS02+0R
                        4Irh3YNmN0eHWjy4TWOY74OTpMgzeyoncWlbjmDgFzNHzF0eQ02b2+Le4ZV9WfrA
                        l+6t8G56alhlzN5/BfIWkUcnOunjEewzfYzlbDkcfVeG3MCOrMQGCTLLULLBOqgh
                        EddLRzMVk8/ggwvYXtq8Fjubyx1N1Uu6RRIlkYceL6rz4/4pIT+Vh8hArr95XIQI
                        UsMqzrXlCNTGq3v18ml3lB7huIVI6vVYCmFhE6pNKvxWy+BRtp5yZElbQwPku96j
                        B1WCW0wAVAJw3dCZF/q23fD5UtFuDAUpKKgu5QIDAQABAoIBAAsnZAqRWNNyCgBw
                        Ic2WRhEylouUhrOe5Pagn9jiiYPTdr7buTZ+epODM2Lb/xFSbYYVeslZlL0JpnSp
                        IQc3ufZC2Zm+DUb2D3PkLTF2bARWtcKFVHBabx2AaQqRW8ums8YeGIR2/kVgcjOO
                        NNk+5ZmEAyIPs9ovo3Lqs0HJqlyHctWRWf40RUx0g6zbmIdBeqhgZujLII8i2gOt
                        h3BOMnc30pi7rw1XQYocFcGuc+SLS6gjjEd6mLfSEe5sxYYVkNrfKnvKIYCjix+o
                        stJ40EpMMsIz1iRf1Zqf3a1bScG7kJxk4sIVURRiTEvf/jfHwJa7J857c/XGYyqd
                        WTe4imkCgYEA6t7Z5dsLDQLCTeEKcNSC1+rZReBMYKOgyHzDUvVCdK9JZyevkgom
                        7tUVU6SJ/lRVEDwlyIFrUynSVl9JWoFe9MsFO1juml02bw85lDanRkFW2/eSv//Q
                        t6y/6BMpBHcczSwUlGKQJztEI7BYEPAAstNFDvqpLnhNfrpLm6kDHU0CgYEAzVXf
                        P7aTAnZlZAjquD9qgq5bVYHR6jOuY13fuv7iQvVSZNbHbKtTDfAnj6vX4Jx6Mszc
                        9b4NuQl/mtXEfpCIpZ4UVRtU4CuWpcpky2Lu+6hRKY7yWWjrGjbmORY6tINGniuO
                        iZ+SCHYnH0d+5BeTincQw9Xvs/H2jdMI+zon6/kCgYALYdFdekgG0WNpMsa2wRW2
                        MQNmh4AsDsHz9Vfh5VDbJsN/5ER151gLqvflncuyL5QPvVp3ZQ42cYhn9yYu5KYd
                        ZtlSDvVBNdM1fp9jt2JT3UbAUjciZttOi2ihboxGUx9qMgCtUDl8LTsYH6uk5nyi
                        Leri1vLr9ZiL06tmV8J24QKBgBOqpuBOV1REDMABDqIpCMWNd5gAACADZS+FPTmG
                        0hsBriuUWrNw0VqjZtae6eVMd3PIJV2wH1Zar4sf8FsC8uOR+XaRUJbL9fjZIsBM
                        MIGQBH4Lf/DKWMJBAdnz5BmH/UqG93b8/pqq3mJjkaa9D1eSwdtP8U513VyGGvUd
                        A9fZAoGBAL8am683j5KHnC42a0oJLm6/EST04KjmIXNrKRpthIC1ne7uEyfAD2uS
                        KBFXjhV2/QhaOjERGOXwgcF5MJo+aftf/uZ34ZTsUSiZebbuURweVu0vkvLCuR51
                        UFlL7vjhkTMkCdgaa5xB8vnFI8+iBcBqZ2kJi9TTvlRpSKzPdLVJ
                        -----END RSA PRIVATE KEY-----
                        """
                ),
                "userId": "987654321123456789",
            },
            1665406800,
            "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk4NzY1NDMyMTk4NzY1NDMyMSIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5ODc2NTQzMjExMjM0NTY3ODkiLCJpYXQiOjE2NjU0MDY4MDAsImF1ZCI6Imh0dHBzOi8veml0YWRlbC5leGFtcGxlLmNvbSIsImlzcyI6Ijk4NzY1NDMyMTEyMzQ1Njc4OSIsImV4cCI6MTY2NTQxMDQwMH0.go3BhDmnjZwcPW3mxgq3Gr5Sqs1M1N86GTZrjZqY26_e-X-FD2QTCP1CM8sWC96uVrJhPkbgNh5Ay6Y9g0CyDnQobskzEKYTV46c4DBDdvQ5MYYceOLhxoOCsdmh8bQRiq3LUjU4GB09ZW7iP-VnSj15Kw6EMYIlfcGWCBhOO8o3bCJEn8ISbXmnLhN5QdwWjQ6R5OFo-e-q9bnIosN6pZJN7k_XwLciM1T-x6wectWY04Goy25tpt2Qzc4PCNyKqGW4x1U1wkp0pnBm0lJ_1GEA_G7yl5R2UEmNpB0t61-UHXCAhH9jvimFqi3zA3AdAmENJOsEPZQN2R4hpUezrQ",  # noqa
        ),
        (
            AppSettings(
                sso_base_url="https://zitadel.example.com",
                key_file="secrets/credentials.json",
                path_prefix="origin",
                client_project_mappers=[
                    ClientProjectMapper(
                        regex=r"^.*(ownCloud).*$",
                        project_id="180582120094172208",
                    )
                ],
            ),
            {
                "type": "serviceaccount",
                "keyId": "987654321123456789",
                "key": textwrap.dedent(
                    """\
                        -----BEGIN RSA PRIVATE KEY-----
                        MIIEowIBAAKCAQEAvGM9Tf3hvJj1uQ11VbC1Q8h/eZ0F3W90O64n+0ZwNvS02+0R
                        4Irh3YNmN0eHWjy4TWOY74OTpMgzeyoncWlbjmDgFzNHzF0eQ02b2+Le4ZV9WfrA
                        l+6t8G56alhlzN5/BfIWkUcnOunjEewzfYzlbDkcfVeG3MCOrMQGCTLLULLBOqgh
                        EddLRzMVk8/ggwvYXtq8Fjubyx1N1Uu6RRIlkYceL6rz4/4pIT+Vh8hArr95XIQI
                        UsMqzrXlCNTGq3v18ml3lB7huIVI6vVYCmFhE6pNKvxWy+BRtp5yZElbQwPku96j
                        B1WCW0wAVAJw3dCZF/q23fD5UtFuDAUpKKgu5QIDAQABAoIBAAsnZAqRWNNyCgBw
                        Ic2WRhEylouUhrOe5Pagn9jiiYPTdr7buTZ+epODM2Lb/xFSbYYVeslZlL0JpnSp
                        IQc3ufZC2Zm+DUb2D3PkLTF2bARWtcKFVHBabx2AaQqRW8ums8YeGIR2/kVgcjOO
                        NNk+5ZmEAyIPs9ovo3Lqs0HJqlyHctWRWf40RUx0g6zbmIdBeqhgZujLII8i2gOt
                        h3BOMnc30pi7rw1XQYocFcGuc+SLS6gjjEd6mLfSEe5sxYYVkNrfKnvKIYCjix+o
                        stJ40EpMMsIz1iRf1Zqf3a1bScG7kJxk4sIVURRiTEvf/jfHwJa7J857c/XGYyqd
                        WTe4imkCgYEA6t7Z5dsLDQLCTeEKcNSC1+rZReBMYKOgyHzDUvVCdK9JZyevkgom
                        7tUVU6SJ/lRVEDwlyIFrUynSVl9JWoFe9MsFO1juml02bw85lDanRkFW2/eSv//Q
                        t6y/6BMpBHcczSwUlGKQJztEI7BYEPAAstNFDvqpLnhNfrpLm6kDHU0CgYEAzVXf
                        P7aTAnZlZAjquD9qgq5bVYHR6jOuY13fuv7iQvVSZNbHbKtTDfAnj6vX4Jx6Mszc
                        9b4NuQl/mtXEfpCIpZ4UVRtU4CuWpcpky2Lu+6hRKY7yWWjrGjbmORY6tINGniuO
                        iZ+SCHYnH0d+5BeTincQw9Xvs/H2jdMI+zon6/kCgYALYdFdekgG0WNpMsa2wRW2
                        MQNmh4AsDsHz9Vfh5VDbJsN/5ER151gLqvflncuyL5QPvVp3ZQ42cYhn9yYu5KYd
                        ZtlSDvVBNdM1fp9jt2JT3UbAUjciZttOi2ihboxGUx9qMgCtUDl8LTsYH6uk5nyi
                        Leri1vLr9ZiL06tmV8J24QKBgBOqpuBOV1REDMABDqIpCMWNd5gAACADZS+FPTmG
                        0hsBriuUWrNw0VqjZtae6eVMd3PIJV2wH1Zar4sf8FsC8uOR+XaRUJbL9fjZIsBM
                        MIGQBH4Lf/DKWMJBAdnz5BmH/UqG93b8/pqq3mJjkaa9D1eSwdtP8U513VyGGvUd
                        A9fZAoGBAL8am683j5KHnC42a0oJLm6/EST04KjmIXNrKRpthIC1ne7uEyfAD2uS
                        KBFXjhV2/QhaOjERGOXwgcF5MJo+aftf/uZ34ZTsUSiZebbuURweVu0vkvLCuR51
                        UFlL7vjhkTMkCdgaa5xB8vnFI8+iBcBqZ2kJi9TTvlRpSKzPdLVJ
                        -----END RSA PRIVATE KEY-----
                        """
                ),
                "userId": "123456789987654321",
            },
            1675406800,
            "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk4NzY1NDMyMTEyMzQ1Njc4OSIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODk5ODc2NTQzMjEiLCJpYXQiOjE2NzU0MDY4MDAsImF1ZCI6Imh0dHBzOi8veml0YWRlbC5leGFtcGxlLmNvbSIsImlzcyI6IjEyMzQ1Njc4OTk4NzY1NDMyMSIsImV4cCI6MTY3NTQxMDQwMH0.nzHc1IGOFCVjZVwY2-tFuPczleLWv1Vitm9zS2yvYn5nHMAG86G2WQRE3Q7J8yXoYuBz0bieeqAr1ze_4BZW_XL5aCDAToDSNTF7g2AHmlp7V0EljlPh9Gv39c2AjSUeuoJN_RXw4iSK6xKb99Wh1AVvuYXNt0Fi6rIbyOYwN2H7s9yaU1QTgd6cCfb0Q6aMWYlA2a28I5Du2DxkKXJgOqqkqeDjP5cC_KgddBKC-2gY6hG2H9mQNKzhzRA5gcPhbgtnsgbAarzb9YZAt4u4K5pEseyDcP3uD8_M2D_r9XNW35oQW7FJGE3TxyE8yCAnZd1dr7H9hrfN9G2BIa3UVA",  # noqa
        ),
    ],
)
def test_with_fixed_timestamp(
    app_settings: AppSettings, key_data: dict, time: int, expected: str
):
    with (
        patch(
            "zitadel_dynamic_client_registrator.settings._app_settings",
            app_settings,
        ),
        patch("time.time", return_value=time),
        patch("builtins.open", mock_open(read_data=json.dumps(key_data))),
    ):
        actual = generate_jwt()

    assert actual == expected
