from fastapi import FastAPI
from starlette import status
from starlette.middleware.cors import CORSMiddleware

from zitadel_dynamic_client_registrator.exceptions import (
    AppWithNameExistsAlready,
    ExhaustedAppRegistrationRetries,
)
from zitadel_dynamic_client_registrator.requests import (
    add_oidc_client,
    build_url,
    get_openid_configuration,
)
from zitadel_dynamic_client_registrator.settings import get_settings
from zitadel_dynamic_client_registrator.tools import generate_random_id
from zitadel_dynamic_client_registrator.types import (
    OIDCAccessTokenType,
    OIDCAppType,
    OIDCAuthMethod,
    OIDCClient,
    OIDCGrantType,
    OIDCResponseType,
    OIDCVersion,
    RegisterIn,
    RegisterOut,
)

fastapi_app = FastAPI()

app = CORSMiddleware(
    fastapi_app,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@fastapi_app.get(
    "/.well-known/openid-configuration",
    response_model=dict,
)
async def openid_configuration():
    configuration = await get_openid_configuration()
    configuration["registration_endpoint"] = build_url("/oauth/v2/register")
    return configuration


@fastapi_app.post(
    "/oauth/v2/register",
    status_code=status.HTTP_201_CREATED,
    response_model=RegisterOut,
)
async def register(client_registration: RegisterIn) -> RegisterOut:
    for mapper in get_settings().client_project_mappers:
        if mapper.regex.fullmatch(client_registration.client_name):
            break
    else:
        raise Exception()  # TODO 400 error
    client = OIDCClient(
        project_id=mapper.project_id,
        name="".join([mapper.prefix, generate_random_id()]),
        redirect_uris=client_registration.redirect_uris,
        response_types=[OIDCResponseType.code],
        grant_types=[OIDCGrantType.authorization_code, OIDCGrantType.refresh_token],
        app_type=OIDCAppType.native,
        auth_method_type=OIDCAuthMethod.none,
        post_logout_redirect_uris=[],
        version=OIDCVersion.v1_0,
        dev_mode=True,
        access_token_type=OIDCAccessTokenType.jwt,
        access_token_role_assertion=False,
        id_token_role_assertion=False,
        id_token_userinfo_assertion=True,
        clock_skew="0s",
        additional_origins=[],
    )

    for _ in range(3):
        try:
            zitadel_application = await add_oidc_client(client)
            break
        except AppWithNameExistsAlready:
            continue
    else:
        raise ExhaustedAppRegistrationRetries()

    return RegisterOut(
        client_id=zitadel_application.client_id,
        client_secret=zitadel_application.client_secret,
        client_secret_expires_at=0,
        redirect_uris=client.redirect_uris,
        grant_types=["authorization_code", "refresh_token"],
    )
