from pathlib import Path
from typing import Pattern

from pydantic import AnyHttpUrl, BaseModel, BaseSettings, FilePath


class ClientProjectMapper(BaseModel):
    regex: Pattern
    project_id: str
    prefix: str = "DCR | "


class AppSettings(BaseSettings):
    sso_base_url: AnyHttpUrl
    key_file: Path
    path_prefix: str
    client_project_mappers: list[ClientProjectMapper]


class _AppSettings(AppSettings):
    key_file: FilePath


_app_settings: AppSettings | None = None


def get_settings() -> AppSettings:
    global _app_settings
    if _app_settings is None:
        _app_settings = _AppSettings()
    return _app_settings
