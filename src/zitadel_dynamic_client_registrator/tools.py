import random

from zitadel_dynamic_client_registrator.settings import get_settings

ID_CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_"


def generate_random_id() -> str:
    return "".join([random.choice(ID_CHARACTERS) for _ in range(0, 11)])


def build_url(*segments: str, trailing_slash: bool = False) -> str:
    sanitized_segments = [segment.strip("/") for segment in segments if segment != ""]
    sanitized_segments.insert(0, get_settings().sso_base_url.rstrip("/"))
    if trailing_slash:
        sanitized_segments.append("")
    return "/".join(sanitized_segments)
