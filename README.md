# Zitadel Dynamic Client Registrator

This is a small web server application to add the capability for
[OAuth 2.0 Dynamic Client Registration](https://www.rfc-editor.org/rfc/rfc7591.html)
to a self-hosted [Zitadel](https://github.com/zitadel/zitadel) instance.

## Why?

The primary purpose of this project is to add this capability until Dynamic Client
Registration has been added to the Zitadel.
This project was born out of the requirement for Dynamic Client Registration when using
Zitadel with [OwnCloud](https://owncloud.com/)'s native client apps.

## For whom is this project

This project is primarily aimed at homelab users.
If you have the need to run this in a production environment or on the cloud version, it
is a better idea to submit a feature request to the Zitadel developers.

## How?

This project works by adding additional routing rules to the reverse proxy in front of
the Zitadel instance.

The returning of the `.well-known/openid-configuration` is delegated to this server
application.
This server fetches the original configuration and adds the registration endpoint to the
response.
When the registration endpoint provided by this server is called, the call is translated
to a Zitadel API call to create a new application on the corresponding project.

## Configuration

Configuration is done through environment variables.

### `KEY_FILE`

Path to the key file containing the private key for the
[Zitadel service user](https://docs.zitadel.com/docs/guides/integrate/serviceusers).

#### Example

```shell
/secrets/credentials.json
```

### `PATH_PREFIX`

The path prefix used to access the original `.well-known/openid-configuration` endpoint.

#### Example

```shell
origin
```

### `SSO_BASE_URL`

The public base URL of your Zitadel instance

#### Example

```
https://zitadel.example.com
```

### `CLIENT_PROJECT_MAPPERS`

This setting will be used to assign incoming client registrations to projects in
Zitadel.

The value is expected to be a JSON list of project mappers.
Each project mappers must contain the `regex` and `project_id` fields.
The `prefix` field is optional.

The `regex` field must contain a valid python regex that identifies an application based
on name.
The application name is evaluated in order of the list until the first entry is found.
If the application name did not match any regex, the register endpoint returns an error
thus the registration fails.

The `project_id` field provides the project_id to which a successfully identified client
should be registered to.
Multiple mapper may be assigned to the same project.

The `prefix` field provides the means to customize the prefix assigned to the
generated application name Zitadel before the randomly generated id to better identify
what kind of client registered the application.

#### Example

```json
[
  {
    "regex": "^.*(ownCloud-android).*$",
    "project_id": "100000000000000000",
    "prefix": "Android | "
  },
  {
    "regex": "^.*(ownCloud).*$",
    "project_id": "100000000000000000",
    "prefix": "Desktop | "
  }
]
```
